package fsmpi.gaming.tnt_crusher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class ExplosionBlockHandler implements Listener {

    private ArrayList<BlockModifier> modifiers;

    public ExplosionBlockHandler(List<BlockModifier> modifiers) {
        this.modifiers = new ArrayList<>(modifiers);
    }
    
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        handleExplosion(event.getLocation(), event.blockList());
    }
    @EventHandler
    public void onBlockExplode(BlockExplodeEvent event) {
        handleExplosion(event.getBlock().getLocation(), event.blockList());
    }
    public void handleExplosion(Location location, List<Block> blockList) {
        HashMap<Location, Block> blockMap = getBlockMap(blockList);

        Collection<Block> targetBlocks = getTargetBlockList(location, blockMap);

        modifyBlocks(targetBlocks);
    }

    private HashMap<Location, Block> getBlockMap(List<Block> blockList) {
        HashMap<Location, Block> blockMap = new HashMap<>();
        for (Block block : blockList) {
            blockMap.put(block.getLocation(), block);
        }
        return blockMap;
    }

    private Collection<Block> getTargetBlockList(Location center, HashMap<Location, Block> explosionMap) {
        HashMap<Location, Block> targetMap = new HashMap<>();
        for (Location location : explosionMap.keySet()) {
            checkLocation(location.clone().add(1, 0, 0), explosionMap, targetMap);
            checkLocation(location.clone().add(-1, 0, 0), explosionMap, targetMap);
            checkLocation(location.clone().add(0, 1, 0), explosionMap, targetMap);
            checkLocation(location.clone().add(0, -1, 0), explosionMap, targetMap);
            checkLocation(location.clone().add(0, 0, 1), explosionMap, targetMap);
            checkLocation(location.clone().add(0, 0, -1), explosionMap, targetMap);
        }
        return targetMap.values();
    }
    
    private void checkLocation(Location location, HashMap<Location, Block> explosionMap, HashMap<Location, Block> targetMap) {
        if (isTargetLocation(location, explosionMap, targetMap)) {
            targetMap.put(location, location.getBlock());
        }
    }
    private boolean isTargetLocation(Location location, HashMap<Location, Block> explosionMap, HashMap<Location, Block> targetMap) {
        return !explosionMap.containsKey(location) && !targetMap.containsKey(location);
    }

    private void modifyBlocks(Collection<Block> blocks) {
        for (Block block : blocks) {
            for (BlockModifier blockModifier : modifiers) {
                blockModifier.modify(block);
            }
        }
    }

    public void addBlockModifier(BlockModifier blockModifier) {
        modifiers.add(blockModifier);
    }
    public void removeBlockModifier(BlockModifier blockModifier) {
        modifiers.remove(blockModifier);
    }
    public List<BlockModifier> getBlockModifierList() {
        return modifiers.subList(0, modifiers.size() - 1);
    }
}

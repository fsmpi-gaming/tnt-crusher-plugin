package fsmpi.gaming.tnt_crusher;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class ChangeBlockModifier implements BlockModifier, ConfigurationSerializable {
    public Material checkFor;
    public Material changeTo;
    public float modifierChance;

    public ChangeBlockModifier(Material checkFor, Material changeTo, float modifierChance) {
        this.checkFor = checkFor;
        this.changeTo = changeTo;
        this.modifierChance = modifierChance;
    }
    public ChangeBlockModifier(Map<String, Object> attributes) {
        checkFor = Material.matchMaterial((String) attributes.get("checkFor"));
        
        changeTo = Material.matchMaterial((String) attributes.get("changeTo"));
        modifierChance = Float.parseFloat((String) attributes.get("modifierChance"));
    }
    @Override
    public void modify(Block block) {
        if (block.getType() == checkFor) {
            Random r = new Random();
            if (r.nextFloat() < modifierChance) {
                block.setType(changeTo);
            }
        }
    }
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("checkFor", checkFor.toString());
        map.put("changeTo", changeTo.toString());
        map.put("modifierChance", modifierChance + "");
        return map;
    }
    
}

package fsmpi.gaming.tnt_crusher;

import org.bukkit.block.Block;

public interface BlockModifier {
    public void modify(Block block);
}

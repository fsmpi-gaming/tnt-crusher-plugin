package fsmpi.gaming.tnt_crusher;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

public class MyPlugin extends JavaPlugin {
    ArrayList<BlockModifier> modifiers;
    @Override
    public void onEnable() {
        ConfigurationSerialization.registerClass(ChangeBlockModifier.class);
        
        List<?> list = getConfig().getList("modifiers", new ArrayList<>());
        modifiers = new ArrayList<>();
        for (Object object : list) {
            if (object instanceof BlockModifier) {
                modifiers.add((BlockModifier) object);
            }
        }

        getServer().getPluginManager().registerEvents(new ExplosionBlockHandler(modifiers), this);
    }

    @Override
    public void onDisable() {
    }

    @Override
    public FileConfiguration getConfig() {
        if (super.getConfig().getCurrentPath().equals("")) {
            super.saveDefaultConfig();
        }
        return super.getConfig();
    }


}
